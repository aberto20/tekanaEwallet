package com.abel.tekanaeWallet.service;

import com.abel.tekanaeWallet.model.request.AuthRequest;
import com.abel.tekanaeWallet.model.response.AuthResponse;
import jakarta.servlet.http.HttpServletRequest;

public interface UserService {
     AuthResponse generateRefreshToken(HttpServletRequest request);
    AuthResponse authenticateUser(AuthRequest authRequest);
}
