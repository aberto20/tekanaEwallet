package com.abel.tekanaeWallet.service.implimentation;

import com.abel.tekanaeWallet.exception.CustomerExistsException;
import com.abel.tekanaeWallet.exception.CustomerNotFoundException;
import com.abel.tekanaeWallet.model.Customer;
import com.abel.tekanaeWallet.model.Users;
import com.abel.tekanaeWallet.model.Wallet;
import com.abel.tekanaeWallet.model.request.NotificationEvent;
import com.abel.tekanaeWallet.model.request.WalletRequest;
import com.abel.tekanaeWallet.model.response.SuccessResponse;
import com.abel.tekanaeWallet.repository.CustomerRepository;
import com.abel.tekanaeWallet.repository.WalletRepository;
import com.abel.tekanaeWallet.service.CustomerService;
import com.abel.tekanaeWallet.utils.NotificationProperties;
import com.google.gson.Gson;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.abel.tekanaeWallet.utils.Generator.generateRandomPassword;
import static com.abel.tekanaeWallet.utils.Generator.generateWalletNumber;

@Service
public class CustomerServiceImpl implements CustomerService {
  Logger logger= LoggerFactory.getLogger(CustomerServiceImpl.class);
    final  CustomerRepository  customerRepository ;
    final WalletRepository walletRepository ;
    final ApplicationEventPublisher eventPublisher ;

    final PasswordEncoder passwordEncoder;
    private final Gson gson;

    public CustomerServiceImpl(CustomerRepository customerRepository, WalletRepository walletRepository, ApplicationEventPublisher eventPublisher, PasswordEncoder passwordEncoder, Gson gson) {
        this.customerRepository = customerRepository;
        this.walletRepository = walletRepository;
        this.eventPublisher = eventPublisher;
        this.passwordEncoder = passwordEncoder;
        this.gson = gson;
    }

    @Override
    public SuccessResponse customerRegistration(@Valid Customer customer) {
        boolean existsCustomer = customerRepository
                .existsCustomerByPhoneNumberOrEmail(customer.getPhoneNumber(), customer.getEmail());
        if(!existsCustomer){
            String password=generateRandomPassword(6);
            Users users=new Users();
            users.setUsername(customer.getEmail());
            users.setPassword(passwordEncoder.encode(password));
            customer.setUser(users);
            customerRepository.save(customer);
            String fullName=customer.getFirstName()
                    .concat(" ").concat(customer.getLastName());
            eventPublisher.publishEvent(
                    new NotificationEvent(this,customer.getEmail(),fullName,gson.toJson(customer), NotificationProperties.EMAIL)
            );
            logger.info(">>>>>>>> Username : {} password : {}",users.getUsername(),password);
            String message = String.format("%s is registered successfully",fullName);
            return new SuccessResponse(HttpStatus.OK.value(), message,customer);
        }

         throw new CustomerExistsException(String.format("Customer with %s already exist",customer.getEmail()));
    }

    @Override
    public SuccessResponse fetchCustomers() {
        List<Customer> customers = customerRepository.findAll();
        return new SuccessResponse(HttpStatus.OK.value(), "Success",customers);
    }

    @Override
    public SuccessResponse fetchCustomerDetails(long customerId) {
        Customer customer = customerRepository.findById(customerId).orElseThrow(()->new CustomerNotFoundException("Customer with id "+customerId+" not found"));
        return new SuccessResponse(HttpStatus.OK.value(), "Success",customer);
    }

    @Override
    public SuccessResponse addCustomerWallet(long customerId, WalletRequest walletRequest) {
        Customer customer = customerRepository.findById(customerId).orElseThrow(()->new CustomerNotFoundException("Customer with id "+customerId+" not found"));
        Wallet wallet=new Wallet();
        wallet.setWalletNumber(generateWalletNumber());
        wallet.setBalance(walletRequest.getInitialDeposit());
        wallet.setCurrency(walletRequest.getCurrency().name());
        wallet.setCustomer(customer);
        walletRepository.save(wallet);
        return new SuccessResponse(HttpStatus.OK.value(), "Success",wallet);
    }

    @Override
    public SuccessResponse fetchCustomerWallet(long customerId) {
        customerRepository.findById(customerId).orElseThrow(()->new CustomerNotFoundException("Customer with id "+customerId+" not found"));
        List<Wallet> wallet = walletRepository.findByCustomerId(customerId);
        return new SuccessResponse(HttpStatus.OK.value(), "Success",wallet);
    }
}
