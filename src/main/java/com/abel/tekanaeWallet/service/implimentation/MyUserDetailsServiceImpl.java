package com.abel.tekanaeWallet.service.implimentation;


import com.abel.tekanaeWallet.exception.GenericErrorException;
import com.abel.tekanaeWallet.exception.UnauthorizedException;
import com.abel.tekanaeWallet.model.Users;
import com.abel.tekanaeWallet.model.request.AuthRequest;
import com.abel.tekanaeWallet.model.request.MyUserDetails;
import com.abel.tekanaeWallet.model.response.AuthResponse;
import com.abel.tekanaeWallet.repository.UsersRepository;
import com.abel.tekanaeWallet.service.UserService;
import com.abel.tekanaeWallet.utils.JwtUtil;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class MyUserDetailsServiceImpl implements UserDetailsService, UserService {
    @Autowired
    private UsersRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    JwtUtil jwtUtil;
    @Override
    public UserDetails loadUserByUsername(String username)  {
        Optional<Users> user = userRepository.findByUsername(username);
        user.orElseThrow(() -> new GenericErrorException("Not found: " + username));
        return user.map(user1 -> {

            List<GrantedAuthority> grantedAuthorities=new ArrayList<>();
            MyUserDetails myUserDetails=new MyUserDetails();
            myUserDetails.setUsername(user1.getUsername());
            myUserDetails.setPassword(user1.getPassword());
            myUserDetails.setEnabled(true);
            myUserDetails.setAccountNonLocked(true);

            myUserDetails.setAuthorities(grantedAuthorities);
           return myUserDetails;
        }).get();
    }
   @Override
    public AuthResponse authenticateUser(AuthRequest authRequest){
        UserDetails userDetails = this.loadUserByUsername(authRequest.getUsername());
        if (passwordEncoder.matches(authRequest.getPassword(),userDetails.getPassword())){
            return jwtUtil.generateToken(userDetails);
        }
        throw new UnauthorizedException("Invalid Credentials");
    }
    @Override
    public AuthResponse generateRefreshToken(HttpServletRequest request) {
        String token = jwtUtil.token(request);
        String username = jwtUtil.extractUsername(token);
        return jwtUtil.generateToken(loadUserByUsername(username));
    }
}
