package com.abel.tekanaeWallet.service.implimentation;

import com.abel.tekanaeWallet.exception.GenericErrorException;
import com.abel.tekanaeWallet.model.Transaction;
import com.abel.tekanaeWallet.model.Wallet;
import com.abel.tekanaeWallet.model.request.TransactionRequest;
import com.abel.tekanaeWallet.model.response.SuccessResponse;
import com.abel.tekanaeWallet.repository.TransactionRepository;
import com.abel.tekanaeWallet.repository.WalletRepository;
import com.abel.tekanaeWallet.service.TransactionService;
import com.abel.tekanaeWallet.utils.TransactionProperties;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.abel.tekanaeWallet.utils.Generator.generateTransactionId;

@Service
public class TransactionServiceImpl implements TransactionService {

    final TransactionRepository transactionRepository;
    final WalletRepository walletRepository;


    public TransactionServiceImpl(TransactionRepository transactionRepository, WalletRepository walletRepository) {
        this.transactionRepository = transactionRepository;
        this.walletRepository = walletRepository;
    }

    @Override
    public SuccessResponse createTransaction(TransactionRequest transactionRequest) {
        String transactionId=generateTransactionId();
        Wallet creditWallet =walletRepository.findById(transactionRequest.getCreditWalletNumber())
                .orElseThrow(()->new GenericErrorException("Wallet number with "+transactionRequest.getCreditWalletNumber()+" not found"));
        Wallet debitWallet =walletRepository.findById(transactionRequest.getDebitWalletNumber())
                .orElseThrow(()->new GenericErrorException("Wallet number with "+transactionRequest.getDebitWalletNumber()+" not found"));
        System.out.println(debitWallet.getBalance().compareTo(transactionRequest.getAmount()));
        System.out.println(debitWallet.getBalance());
        if (debitWallet.getBalance().compareTo(transactionRequest.getAmount()) >= 0){
            //create debit transaction
            debitWallet.setBalance(debitWallet.getBalance().subtract(transactionRequest.getAmount()));
            walletRepository.save(debitWallet);

            Transaction debitTransaction =new Transaction();
            debitTransaction.setTransactionType(TransactionProperties.DEBIT);
            debitTransaction.setAmount(transactionRequest.getAmount());
            debitTransaction.setBalance(debitWallet.getBalance());
            debitTransaction.setWallet(debitWallet);
            debitTransaction.setDescription(transactionRequest.getDescription());
            debitTransaction.setTransactionId(transactionId);
            transactionRepository.save(debitTransaction);

           //create credit transaction
            creditWallet.setBalance(creditWallet.getBalance().add(transactionRequest.getAmount()));
            walletRepository.save(creditWallet);
            Transaction creditTransaction =new Transaction();
            creditTransaction.setTransactionType(TransactionProperties.CREDIT);
            creditTransaction.setAmount(transactionRequest.getAmount());
            creditTransaction.setBalance(creditWallet.getBalance());
            creditTransaction.setWallet(creditWallet);
            creditTransaction.setDescription(transactionRequest.getDescription());
            creditTransaction.setTransactionId(transactionId);
            transactionRepository.save(creditTransaction);

            return new SuccessResponse(HttpStatus.OK.value(), "Success",creditTransaction);
        }

        throw new GenericErrorException("No enough funds");





    }

    @Override
    public SuccessResponse getWalletStatement(String walletNumber, String startDate, String endDate) throws ParseException {
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date start=sdf.parse(startDate.concat(" 00:00:00"));
        Date end=sdf.parse(endDate.concat(" 23:59:59"));
        List<Transaction> transactions = transactionRepository.findByWalletWallet(walletNumber, start, end);
        return new SuccessResponse(HttpStatus.OK.value(), "Success",transactions);
    }

    @Override
    public SuccessResponse walletBalance(String walletNumber) {
        Wallet wallet = walletRepository.findById(walletNumber)
                .orElseThrow(() -> new GenericErrorException("Wallet number with " + walletNumber + " not found"));
        return new SuccessResponse(HttpStatus.OK.value(), "Success",wallet);
    }

}
