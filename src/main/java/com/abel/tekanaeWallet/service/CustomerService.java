package com.abel.tekanaeWallet.service;

import com.abel.tekanaeWallet.model.Customer;
import com.abel.tekanaeWallet.model.Wallet;
import com.abel.tekanaeWallet.model.request.WalletRequest;
import com.abel.tekanaeWallet.model.response.SuccessResponse;
import com.abel.tekanaeWallet.repository.CustomerRepository;
import jakarta.validation.Valid;

public interface CustomerService {

    SuccessResponse customerRegistration(@Valid  Customer customer);
    SuccessResponse fetchCustomers();
    SuccessResponse fetchCustomerDetails(long customerId);
    SuccessResponse addCustomerWallet(long customerId ,  WalletRequest walletRequest);
    SuccessResponse fetchCustomerWallet(long customerId);
}
