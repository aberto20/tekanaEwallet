package com.abel.tekanaeWallet.service;

import com.abel.tekanaeWallet.model.request.TransactionRequest;
import com.abel.tekanaeWallet.model.response.SuccessResponse;

import java.text.ParseException;

public interface TransactionService {
    SuccessResponse createTransaction(TransactionRequest transactionRequest);
    SuccessResponse getWalletStatement(String walletNumber, String startDate, String endDate) throws ParseException;
    SuccessResponse walletBalance(String walletNumber);
}
