package com.abel.tekanaeWallet.model;


import com.abel.tekanaeWallet.utils.TransactionProperties;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Data;

import java.math.BigDecimal;

@Table
@Entity
public class Transaction extends BasicEntity {

    BigDecimal amount=BigDecimal.ZERO;
    TransactionProperties transactionType;

    BigDecimal balance=BigDecimal.ZERO;

    String description;
    String transactionId;


    @ManyToOne
    Wallet wallet;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public TransactionProperties getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionProperties transactionType) {
        this.transactionType = transactionType;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Wallet getWallet() {
        return wallet;
    }

    public void setWallet(Wallet wallet) {
        this.wallet = wallet;
    }
}
