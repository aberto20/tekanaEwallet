package com.abel.tekanaeWallet.model;


import jakarta.persistence.*;


import java.math.BigDecimal;

@Table
@Entity
public class Wallet {
    @Id
    @Column(length = 50)
    String walletNumber;
    @Column(length = 10)
    String currency;
    BigDecimal balance=BigDecimal.ZERO;

    @ManyToOne
    Customer customer;

    public String getWalletNumber() {
        return walletNumber;
    }

    public void setWalletNumber(String walletNumber) {
        this.walletNumber = walletNumber;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
}
