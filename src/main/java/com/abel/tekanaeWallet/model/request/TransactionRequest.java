package com.abel.tekanaeWallet.model.request;


import lombok.Data;

import java.math.BigDecimal;


@Data
public class TransactionRequest {
    BigDecimal amount=BigDecimal.ZERO;
    String debitWalletNumber;
    String debitWalletCurrency;
    String creditWalletNumber;
    String creditWalletCurrency;
    String description;
}
