package com.abel.tekanaeWallet.model.request;

import com.abel.tekanaeWallet.utils.NotificationProperties;
import org.springframework.context.ApplicationEvent;

public class NotificationEvent extends ApplicationEvent {
    private String receiver;
    private String name;
    private String payload;

    NotificationProperties notificationType;

    public NotificationEvent(Object source, String receiver, String name, String payload, NotificationProperties notificationType) {
        super(source);
        this.receiver=receiver;
        this.payload=payload;
        this.name=name;
        this.notificationType=notificationType;
    }



    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public NotificationProperties getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(NotificationProperties notificationType) {
        this.notificationType = notificationType;
    }
}
