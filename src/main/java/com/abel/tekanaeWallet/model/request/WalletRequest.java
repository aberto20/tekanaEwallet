package com.abel.tekanaeWallet.model.request;

import com.abel.tekanaeWallet.utils.CurrencyProperties;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class WalletRequest {
    CurrencyProperties currency;
    BigDecimal initialDeposit =BigDecimal.ZERO;
}
