package com.abel.tekanaeWallet.model.response;

import java.util.Date;

public record SuccessResponse( Integer status,String message,Object data ){
    static String timestamps=new Date().toString();
}
