package com.abel.tekanaeWallet.model.response;

import lombok.Data;

@Data
public class ValidationError {
    String field;
    String message;
}
