package com.abel.tekanaeWallet.model.response;

import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
public class ErrorResponse {
    public String error;
    Integer status;
    String message;
    String timestamps = new Date().toString();
    List<ValidationError> errors=new ArrayList<>();
}
