package com.abel.tekanaeWallet.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;

@Table
@Entity
public class Customer extends BasicEntity {
    @NotBlank
    @Column(length = 100)
    String firstName;

    @NotBlank
    @Column(length = 100)
    String lastName;
    @NotBlank
    @Pattern(regexp = "^[0-9]{12}$")
    @Column(length = 12,unique = true)
    String phoneNumber;
    @NotBlank
    @Email
    @Column(length = 100,unique = true)
    String email;
    @Column(length = 150)
    String country;

    @JsonIgnore
    @JsonIgnoreProperties(allowSetters = true)
    @Schema(hidden = true)
    @ManyToOne(cascade = CascadeType.ALL)
    Users user;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }
}
