package com.abel.tekanaeWallet.utils;

import java.text.SimpleDateFormat;
import java.util.Random;

public class Generator {
    private static final String CHAR_LOWER = "abcdefghijklmnopqrstuvwxyz";
    private static final String CHAR_UPPER = CHAR_LOWER.toUpperCase();
    private static final String NUMBER = "0123456789";
    private static final String OTHER_CHAR = "!@#$%&*()_+-=[]?";
    private static final String PASSWORD_ALLOW_BASE = CHAR_LOWER + CHAR_UPPER + NUMBER+OTHER_CHAR ;
    public static String generateTransactionId() {
        long timestamp = System.currentTimeMillis();
        Random random = new Random();
        int randomInt = random.nextInt(100); // Adjust the range as needed
        return "TR"+timestamp + "" + randomInt;
    }
    public static String generateWalletNumber() {
        long timestamp = System.currentTimeMillis();

        Random random = new Random();
        int randomInt = random.nextInt(1000); // Adjust the range as needed
        return new SimpleDateFormat("yyyMMdd").format(timestamp) + "" + randomInt;
    }
    public  static String generateRandomPassword(int length){

        Random random = new Random();
        if (length < 1) throw new IllegalArgumentException("Length must be at least 1.");

        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            int randomIndex = random.nextInt(PASSWORD_ALLOW_BASE.length());
            sb.append(PASSWORD_ALLOW_BASE.charAt(randomIndex));
        }
        return sb.toString();
    }
}
