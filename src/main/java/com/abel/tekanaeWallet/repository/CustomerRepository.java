package com.abel.tekanaeWallet.repository;


import com.abel.tekanaeWallet.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface CustomerRepository extends JpaRepository<Customer,Long> {
    boolean existsCustomerByPhoneNumberOrEmail(String phoneNumber ,String email);
}
