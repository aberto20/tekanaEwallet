package com.abel.tekanaeWallet.repository;

import com.abel.tekanaeWallet.model.Wallet;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface WalletRepository  extends JpaRepository<Wallet,String> {

    List<Wallet> findByCustomerId(long customerId);
}
