package com.abel.tekanaeWallet.repository;

import com.abel.tekanaeWallet.model.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface TransactionRepository extends JpaRepository<Transaction,Long> {

    @Query("SELECT N FROM Transaction N WHERE N.wallet.walletNumber=:walletNumber and N.createdAt between :startDate and :endDate " )
    List<Transaction> findByWalletWallet(String walletNumber, Date startDate, Date endDate);
}
