package com.abel.tekanaeWallet.events;

import com.abel.tekanaeWallet.model.request.NotificationEvent;
import com.abel.tekanaeWallet.utils.NotificationProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class Notification {
    Logger logger= LoggerFactory.getLogger(Notification.class);
    @EventListener
    public void sendNotification(NotificationEvent notificationEvent){
        if (notificationEvent.getNotificationType().equals(NotificationProperties.EMAIL)){
            logger.info(">>> Sending registration confirmation email to {}",notificationEvent.getReceiver());
        }else if (notificationEvent.getNotificationType().equals(NotificationProperties.SMS)){
            logger.info(">>> Sending registration confirmation SMS to {}",notificationEvent.getReceiver());
        }else {
            logger.error(">>> No notification type found ");
        }
    }
}
