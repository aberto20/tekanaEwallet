package com.abel.tekanaeWallet.controller;

import com.abel.tekanaeWallet.exception.GenericErrorException;
import com.abel.tekanaeWallet.model.Transaction;
import com.abel.tekanaeWallet.model.Wallet;
import com.abel.tekanaeWallet.model.request.TransactionRequest;
import com.abel.tekanaeWallet.model.response.ErrorResponse;
import com.abel.tekanaeWallet.model.response.SuccessResponse;
import com.abel.tekanaeWallet.service.TransactionService;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.Date;

@RestController
@RequestMapping("/transaction")
public class TransactionController {
    final TransactionService transactionService;

    public TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }


    @PostMapping
    public ResponseEntity<SuccessResponse> createTransaction(@RequestBody @Valid TransactionRequest transactionRequest){
        return ResponseEntity.ok(transactionService.createTransaction(transactionRequest));

    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Ok", content =
                    {@Content(mediaType = "application/json", schema =
                    @Schema(implementation = Transaction.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request payload supplied", content =
                    {@Content(mediaType = "application/json", schema =
                    @Schema(implementation = ErrorResponse.class))}),
            @ApiResponse(responseCode = "500", description = "Internal server error", content =
                    {@Content(mediaType = "application/json", schema =
                    @Schema(implementation = ErrorResponse.class))})})
    @GetMapping("/history/{walletNumber}/{startDate}/{endDate}")
    @Operation(summary = "Fetch wallet transaction history  ")
    public ResponseEntity<SuccessResponse> transactionHistory( @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd")  String startDate,@PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd")  String endDate, @PathVariable String walletNumber){
        try {
            return ResponseEntity.ok(transactionService.getWalletStatement(walletNumber,startDate,endDate));
        } catch (ParseException e) {
            throw new GenericErrorException(e.getMessage());
        }

    }


    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Ok", content =
                    {@Content(mediaType = "application/json", schema =
                    @Schema(implementation = Wallet.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request payload supplied", content =
                    {@Content(mediaType = "application/json", schema =
                    @Schema(implementation = ErrorResponse.class))}),
            @ApiResponse(responseCode = "500", description = "Internal server error", content =
                    {@Content(mediaType = "application/json", schema =
                    @Schema(implementation = ErrorResponse.class))})})
    @Operation(summary = "Fetch wallet details ")
    @GetMapping("/wallet/{walletNumber}")
    public ResponseEntity<SuccessResponse> walletDetails( @PathVariable String walletNumber){
        return ResponseEntity.ok(transactionService.walletBalance(walletNumber));

    }
}
