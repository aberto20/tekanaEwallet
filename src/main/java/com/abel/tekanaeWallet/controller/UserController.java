package com.abel.tekanaeWallet.controller;

import com.abel.tekanaeWallet.model.request.AuthRequest;
import com.abel.tekanaeWallet.model.response.AuthResponse;
import com.abel.tekanaeWallet.model.response.ErrorResponse;
import com.abel.tekanaeWallet.model.response.SuccessResponse;
import com.abel.tekanaeWallet.service.UserService;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
public class UserController {

    @Autowired
    UserService userService;

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Ok", content =
                    {@Content(mediaType = "application/json", schema =
                    @Schema(implementation = AuthResponse.class))}),
            @ApiResponse(responseCode = "401", description = "Invalid Credentials", content =
                    {@Content(mediaType = "application/json", schema =
                    @Schema(implementation = ErrorResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request payload supplied", content =
                    {@Content(mediaType = "application/json", schema =
                    @Schema(implementation = ErrorResponse.class))}),
            @ApiResponse(responseCode = "500", description = "Internal server error", content =
                    {@Content(mediaType = "application/json", schema =
                    @Schema(implementation = ErrorResponse.class))})})
    @PostMapping("/login")
    public ResponseEntity<SuccessResponse> customerLogin(@RequestBody @Valid AuthRequest authRequest) {
        AuthResponse authResponse = userService.authenticateUser(authRequest);
        return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(),"Success",authResponse));
    }
    @PostMapping("/refresh")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Ok", content =
                    {@Content(mediaType = "application/json", schema =
                    @Schema(implementation = AuthResponse.class))}),
            @ApiResponse(responseCode = "401", description = "Invalid Credentials", content =
                    {@Content(mediaType = "application/json", schema =
                    @Schema(implementation = ErrorResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request payload supplied", content =
                    {@Content(mediaType = "application/json", schema =
                    @Schema(implementation = ErrorResponse.class))}),
            @ApiResponse(responseCode = "500", description = "Internal server error", content =
                    {@Content(mediaType = "application/json", schema =
                    @Schema(implementation = ErrorResponse.class))})})
    public ResponseEntity<SuccessResponse> refreshToken(HttpServletRequest request) {
        AuthResponse authResponse = userService.generateRefreshToken(request);
        return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(),"Success",authResponse));
    }
}
