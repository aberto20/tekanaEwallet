package com.abel.tekanaeWallet.controller;

import com.abel.tekanaeWallet.model.Customer;
import com.abel.tekanaeWallet.model.Wallet;
import com.abel.tekanaeWallet.model.request.WalletRequest;
import com.abel.tekanaeWallet.model.response.ErrorResponse;
import com.abel.tekanaeWallet.service.CustomerService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("customer")
public class CustomerController {


   final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Ok", content =
                    {@Content(mediaType = "application/json", schema =
                    @Schema(implementation = Customer.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request payload supplied", content =
                    {@Content(mediaType = "application/json", schema =
                    @Schema(implementation = ErrorResponse.class))}),
            @ApiResponse(responseCode = "500", description = "Internal server error", content =
                    {@Content(mediaType = "application/json", schema =
                    @Schema(implementation = ErrorResponse.class))})})
    @PostMapping("/registration")
    @Operation(summary = "Customer registration")
    public ResponseEntity<?> customerRegistration(
            @Parameter(in = ParameterIn.DEFAULT, description = "Customer request body", required = true, content = @Content(schema = @Schema(implementation = Customer.class)))
            @RequestBody Customer customer) {
        return ResponseEntity.ok(customerService.customerRegistration(customer));
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Ok", content =
                    {@Content(mediaType = "application/json", schema =
                    @Schema(implementation = Customer.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request payload supplied", content =
                    {@Content(mediaType = "application/json", schema =
                    @Schema(implementation = ErrorResponse.class))}),
            @ApiResponse(responseCode = "500", description = "Internal server error", content =
                    {@Content(mediaType = "application/json", schema =
                    @Schema(implementation = ErrorResponse.class))})})
    @GetMapping
    @Operation(summary = "Fetch Customers ")
    public ResponseEntity<?> fetchCustomers() {
        return ResponseEntity.ok(customerService.fetchCustomers());
    }
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Ok", content =
                    {@Content(mediaType = "application/json", schema =
                    @Schema(implementation = Customer.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request payload supplied", content =
                    {@Content(mediaType = "application/json", schema =
                    @Schema(implementation = ErrorResponse.class))}),
            @ApiResponse(responseCode = "500", description = "Internal server error", content =
                    {@Content(mediaType = "application/json", schema =
                    @Schema(implementation = ErrorResponse.class))})})
    @GetMapping("/{customerId}")
    @Operation(summary = " Customer details ")
    public ResponseEntity<?> getCustomerDetails(@PathVariable long customerId) {
        return ResponseEntity.ok(customerService.fetchCustomerDetails(customerId));
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Ok", content =
                    {@Content(mediaType = "application/json", schema =
                    @Schema(implementation = Wallet.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request payload supplied", content =
                    {@Content(mediaType = "application/json", schema =
                    @Schema(implementation = ErrorResponse.class))}),
            @ApiResponse(responseCode = "500", description = "Internal server error", content =
                    {@Content(mediaType = "application/json", schema =
                    @Schema(implementation = ErrorResponse.class))})})
    @PostMapping("/{customerId}/wallet")
    @Operation(summary = "add Customer wallet ")
    public ResponseEntity<?> addWallet(@PathVariable long customerId, @RequestBody WalletRequest walletRequest) {
        return ResponseEntity.ok(customerService.addCustomerWallet(customerId,walletRequest));
    }
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Ok", content =
                    {@Content(mediaType = "application/json", schema =
                    @Schema(implementation = Wallet.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request payload supplied", content =
                    {@Content(mediaType = "application/json", schema =
                    @Schema(implementation = ErrorResponse.class))}),
            @ApiResponse(responseCode = "500", description = "Internal server error", content =
                    {@Content(mediaType = "application/json", schema =
                    @Schema(implementation = ErrorResponse.class))})})
    @GetMapping("/{customerId}/wallets")
    @Operation(summary = "Fetch customer wallet  ")
    public ResponseEntity<?> getCustomerWallet(@PathVariable long customerId) {
        return ResponseEntity.ok(customerService.fetchCustomerWallet(customerId));
    }
}
