package com.abel.tekanaeWallet.exception;


import com.abel.tekanaeWallet.model.response.ErrorResponse;
import com.abel.tekanaeWallet.model.response.ValidationError;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.context.request.WebRequest;

import javax.management.relation.RoleNotFoundException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;

@RestControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class GlobalCustomException {
    ObjectMapper objectMapper=new ObjectMapper();
 Logger logger= LoggerFactory.getLogger(this.getClass());
    @ExceptionHandler(MethodArgumentNotValidException.class)
    private ResponseEntity<ErrorResponse> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex
            ) throws JsonProcessingException {
        ErrorResponse errorResponse = new ErrorResponse();
        List<ValidationError> validationError = new ArrayList<>();
        for (FieldError fieldError : ex.getBindingResult().getFieldErrors()) {
            ValidationError error =new  ValidationError();
            error.setField(fieldError.getField());
            error.setMessage(fieldError.getDefaultMessage());
            validationError.add(error);

        }
        errorResponse.setStatus(HttpStatus.BAD_REQUEST.value());
        errorResponse.setError("BAD_REQUEST");
        errorResponse.setErrors(validationError);
        logger.error("Error >> "+objectMapper.writeValueAsString(errorResponse));
        return ResponseEntity.badRequest().body(errorResponse);
    }

    @ExceptionHandler({InternalError.class,HttpServerErrorException.InternalServerError.class})
    ResponseEntity<ErrorResponse> internalErrorExceptions(RuntimeException ex , WebRequest webRequest) {
        ErrorResponse ErrorResponse=new  ErrorResponse();
        ErrorResponse.setMessage("SOMETHING WENT WRONG");
        ErrorResponse.setError(HttpStatus.INTERNAL_SERVER_ERROR.name());
        ErrorResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        logger.error("Error >> "+ex.getMessage());
        return ResponseEntity.internalServerError().body(ErrorResponse);
    }
    @ExceptionHandler({RoleNotFoundException.class})
    ResponseEntity<ErrorResponse> roleExceptions(RoleNotFoundException ex , WebRequest webRequest) {
        ErrorResponse ErrorResponse=new  ErrorResponse();
        ErrorResponse.setMessage(ex.getMessage());
        ErrorResponse.setError(HttpStatus.BAD_REQUEST.name());
        ErrorResponse.setStatus(HttpStatus.BAD_REQUEST.value());
        logger.error("Error >> "+ex.getMessage());
        return ResponseEntity.internalServerError().body(ErrorResponse);
    }
    @ExceptionHandler(RuntimeException.class)
    ResponseEntity<ErrorResponse> badRequestExceptions(RuntimeException ex ) {
        HttpStatus httpStatus;
        ErrorResponse ErrorResponse;
        if (ex instanceof CustomerExistsException){
             ErrorResponse=new  ErrorResponse();
            ErrorResponse.setMessage(ex.getMessage());
            ErrorResponse.setError(HttpStatus.BAD_REQUEST.name());
            ErrorResponse.setStatus(HttpStatus.BAD_REQUEST.value());
            httpStatus=HttpStatus.BAD_REQUEST;
        }else  if (ex instanceof CustomerNotFoundException){
             ErrorResponse=new  ErrorResponse();
            ErrorResponse.setMessage(ex.getMessage());
            ErrorResponse.setError(HttpStatus.BAD_REQUEST.name());
            ErrorResponse.setStatus(HttpStatus.BAD_REQUEST.value());
            httpStatus=HttpStatus.BAD_REQUEST;
        }else  if (ex instanceof UserAlreadyExistsException){
             ErrorResponse=new  ErrorResponse();
            ErrorResponse.setMessage(ex.getMessage());
            ErrorResponse.setError(HttpStatus.BAD_REQUEST.name());
            ErrorResponse.setStatus(HttpStatus.BAD_REQUEST.value());
            httpStatus=HttpStatus.BAD_REQUEST;
        }else  if (ex instanceof UserNotFoundException){
             ErrorResponse=new  ErrorResponse();
            ErrorResponse.setMessage(ex.getMessage());
            ErrorResponse.setError(HttpStatus.BAD_REQUEST.name());
            ErrorResponse.setStatus(HttpStatus.BAD_REQUEST.value());
            httpStatus=HttpStatus.BAD_REQUEST;
        }else  if (ex instanceof GenericErrorException){
             ErrorResponse=new  ErrorResponse();
            ErrorResponse.setMessage(ex.getMessage());
            ErrorResponse.setError(HttpStatus.BAD_REQUEST.name());
            ErrorResponse.setStatus(HttpStatus.BAD_REQUEST.value());
            httpStatus=HttpStatus.BAD_REQUEST;
        }else  if (ex instanceof UnauthorizedException){
             ErrorResponse=new  ErrorResponse();
            ErrorResponse.setMessage(ex.getMessage());
            ErrorResponse.setError(HttpStatus.UNAUTHORIZED.name());
            ErrorResponse.setStatus(HttpStatus.UNAUTHORIZED.value());
            httpStatus=HttpStatus.UNAUTHORIZED;
        }
        else {
             ErrorResponse=new  ErrorResponse();
            ErrorResponse.setMessage("WRONG REQUEST");
            ErrorResponse.setError(HttpStatus.INTERNAL_SERVER_ERROR.name());
            ErrorResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            httpStatus=HttpStatus.INTERNAL_SERVER_ERROR;
        }
        logger.error("Error >> "+ex.getMessage());
        return ResponseEntity.status(httpStatus).body(ErrorResponse);
    }
    @ExceptionHandler(DataIntegrityViolationException.class)
    ResponseEntity<ErrorResponse> badRequestExceptions(DataIntegrityViolationException ex, SQLIntegrityConstraintViolationException sqlEx, WebRequest webRequest) {
        ErrorResponse ErrorResponse=new  ErrorResponse();
        ErrorResponse.setMessage(sqlEx.getMessage().split("for key ")[0].replace("Duplicate entry","")+" Exists");
        ErrorResponse.setError(HttpStatus.BAD_REQUEST.name());
        ErrorResponse.setStatus(HttpStatus.BAD_REQUEST.value());
        logger.error("Error >> "+ex.getMessage());
        return ResponseEntity.internalServerError().body(ErrorResponse);
    }
}
