package com.abel.tekanaeWallet.exception;

public class GenericErrorException extends RuntimeException{
    public GenericErrorException(String message) {
        super(message);
    }
}
