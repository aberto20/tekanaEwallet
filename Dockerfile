FROM openjdk:17-jdk-bullseye
WORKDIR /app
ADD /target/tekana-eWallet.jar tekana-eWallet.jar
ENTRYPOINT ["java","-jar","tekana-eWallet.jar"]