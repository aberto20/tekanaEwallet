# Tekana eWallet 
Tekana eWallet,
is a System that allows customers to register and get credentials
that will allow them to login in their account and add the wallet,
with Tekana eWallet you can have a way of making  transaction,
by sending credit to any member whose registered in the system,

### step-by-step strategy for revamping the "Tekana-eWallet" 
1. Requirement Analysis and Understanding
   * Getting detailed requirements from business unit
   * Go through existing system and identify pain points and areas for improvement
   * Understand existing database structure and how data flows
2. Technology stack
   * Do some research on which modern technology stack that can be easily scalable
   * I have considered Java 17 / spring boot 3
3. System design architecture
   * Design a system architecture that can easily scalable and maintainable( e.g :Microservices architecture)
   * Use containerization(e.g :Docker) and orchestration for deployment 
   * Implementation of Api to enable easy integration with the front end and future third-party applications.
4. Development methodology Approach
   * Implement an Agile Development methodology with regular sprint and continuous integration
5. Implementation of practices and Security
   * Write clean code that can be easily readable by other developers
   * implement security the best practice in all stages of developments
6. Collaboration
   * Collaborate closely with other team with front-end,UI AND UX designers
   * define api contracts to facilitate communication between front-end and back-end teams.
7. Scalability and performance optimization
   * Implementation of caching mechanism and optimize database queries
   * Test performance using Load testing tool to identify potential bottlenecks
8. User Acceptance Testing (UAT)
   * Conduct thorough UAT with a selected group of users to ensure the system meets the requirements and is user-friendly
9. Pilot Launch
   * Launch the pilot system in a controlled environment, collecting feedback from users and stakeholders for further enhancements.

# Getting started with Tekana eWallet

## Requirements for to run this project
* have docker installed
* Have Intellij IDEA or other tool for build and install Maven project
* Application  will be running on port 8080

## Environment variable for application
If you run application with docker compose you will use .env file that is located on the root of project
If you run application with IDEA you will use env.dev file that is located on the root of project,
and before you run it you will need to configure environment on your IDEA


For building Maven Project
  ```
    mvn clean install

  ```
For building and run postgres database
  ``` 
  docker compose up -d --build  postgres
  ```
For building docker image and run tekana-wallet service
  ``` 
  docker compose up -d --build  tekana-wallet
  ```
For viewing logs tekana-wallet service
  ```
  docker compose logs -f tekana-wallet 
  ```
For stopping a service
  ```
  docker compose down 
  ```
## Stack used
  * For development of this System I have used java 17 and spring boot 3 framework,
  and for packaging and deployment I have used Docker
  * Postgres for database
 ## Swagger documentation
   * For Accessing documentation go to http://localhost:8080/swagger-ui/index.html
   * U will need to register a customer and get username and password in the logs,
    normally I was supposed to be sent to email or SMS.
   * Swagger documentation will look like :
    ![img.png](img.png)
## Database
   * database name : tekana_ewallet 
   * Tables: ![img_1.png](img_1.png)
   * users : ![img_2.png](img_2.png)
   * customer : ![img_3.png](img_3.png)
   * Wallet : ![img_4.png](img_4.png)
   * Transaction: ![img_5.png](img_5.png)



  
